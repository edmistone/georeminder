package se2.georeminder.Database;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

@Dao
public interface IReminderDAO
{
    @Insert
    void addReminder(ReminderEntity reminder);
    
    @Query("SELECT * FROM reminders")
    LiveData<List<ReminderEntity>> getAllReminders();
    
    @Query("SELECT * FROM reminders WHERE reminderId = :id")
    ReminderEntity getReminder(int id);
    
    @Update
    void updateReminder(ReminderEntity reminder);
    
    @Delete
    void deleteReminder(ReminderEntity reminder);
}
