package se2.georeminder.Database;

import android.content.Context;
import android.os.AsyncTask;

import java.util.Calendar;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;
import androidx.sqlite.db.SupportSQLiteDatabase;

@Database(entities = {ReminderEntity.class}, version = 3, exportSchema = false)
@TypeConverters(DateConverter.class)
public abstract class ReminderDatabase extends RoomDatabase
{
    // Defines the database instance here instead of the main activity
    //  for more flexibility
    private static ReminderDatabase instance;
    
    // Creates the link to our reminder data access object
    public abstract IReminderDAO ReminderDAO();
    
    // If no instance is created this method will instantiate a new instance
    //  as well as ensure multiple instances of the db aren't created to avoid app
    //  crashes(synchronized).
    public static synchronized ReminderDatabase getInstance(Context context)
    {
        if(instance == null)
        {
            // Creates new instance. If version number is changed, it simply destroys the
            //  old db and replaces with a new one.
            instance = Room.databaseBuilder(context, ReminderDatabase.class,
                    "reminder_database")
                    .fallbackToDestructiveMigration()
                    .addCallback(roomCallback)
                    .build();
        }
        return instance;
    }
    
    // Creates a call back method that will activate the populate db method.
    private static RoomDatabase.Callback roomCallback = new RoomDatabase.Callback()
    {
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db)
        {
            super.onCreate(db);
            new PopulateDbAsyncTask(instance).execute();
        }
    };
    
    private static class PopulateDbAsyncTask extends AsyncTask<Void, Void, Void>
    {
        private IReminderDAO reminderDAO;
        
        private PopulateDbAsyncTask(ReminderDatabase db)
        {
            reminderDAO = db.ReminderDAO();
        }
        
        @Override
        protected Void doInBackground(Void... voids)
        {
            int tempNum = 0;
            for (int i = 0; i < 25; i++) {
                tempNum = 0;
                tempNum = i * 69;
                reminderDAO.addReminder(
                        new ReminderEntity("Test Name " + i,
                                "The cake is a lie " + i,
                                "WHITE",
                                tempNum + "Test Street Drive",
                                (float)tempNum,
                                Calendar.getInstance().getTime()));
            }
            
            return null;
        }
    }
}
