package se2.georeminder.Database;

import java.util.Date;

import androidx.room.TypeConverter;

//Date converter to modify dates from java date object <--> SQL date object
public class DateConverter
{
    @TypeConverter
    public static Date fromTimestamp(Long value) {
        return value == null ? null : new Date(value);
    }
    
    @TypeConverter
    public static Long dateToTimestamp(Date date) {
        return date == null ? null : date.getTime();
    }
}
