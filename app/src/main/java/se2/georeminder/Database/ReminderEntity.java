package se2.georeminder.Database;

import org.jetbrains.annotations.NotNull;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "Reminders")
public class ReminderEntity
{
    @NonNull @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "reminderId")
    private int id;
    
    @NonNull @ColumnInfo(name = "Reminder_Name")
    private String name;
    
    @NonNull @ColumnInfo(name = "Reminder_Description")
    private String description;
    
    @NonNull @ColumnInfo(name = "Reminder_Color")
    private String color;
    
    @NonNull @ColumnInfo(name = "Reminder_Address")
    private String address;
    
    @NonNull @ColumnInfo(name = "Reminder_Distance")
    private Float distance;
    
    @NonNull @ColumnInfo(name = "Reminder_Creation_Date")
    private Date date;
    
    @NonNull @ColumnInfo(name = "Reminder_Expiration_Date")
    private Date expiration;

    public ReminderEntity(@NonNull String name, @NonNull String description, @NonNull String color,
                          @NonNull String address, @NonNull Float distance, @NonNull Date expiration)
    {
        setName(name);
        setDescription(description);
        setColor(color);
        setAddress(address);
        setDate(modifyDate(Calendar.getInstance().getTime()));
        setDistance(distance);
        setExpiration(modifyDate(expiration));
    }

    //Setters & Getters for the Entity
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @NonNull
    public String getName() {
        return name;
    }

    public void setName(@NonNull String name) {
        this.name = name;
    }

    @NonNull
    public String getDescription() {
        return description;
    }

    public void setDescription(@NonNull String description) {
        this.description = description;
    }

    @NonNull
    public String getColor() {
        return color;
    }

    public void setColor(@NonNull String color) {
        this.color = color;
    }

    @NonNull
    public String getAddress() {
        return address;
    }

    public void setAddress(@NonNull String address) {
        this.address = address;
    }
    
    @NonNull
    public Date getDate()
    {
        return date;
    }
    
    public void setDate(Date date)
    {
        this.date = date;
    }
    
    @NonNull
    public Float getDistance()
    {
        return distance;
    }
    
    public void setDistance(@NonNull Float distance)
    {
        this.distance = distance;
    }
    
    @NonNull
    public Date getExpiration()
    {
        return expiration;
    }
    
    public void setExpiration(@NonNull Date expiration)
    {
        this.expiration = expiration;
    }
    
    public Date modifyDate(Date date)
    {
        // Changes Date to DateFormat for formatting -> accounts for timezone
        //  -> changes back to Date object -> sets the DB value.
        DateFormat formatter = DateFormat.getDateInstance(DateFormat.FULL);
        formatter.setTimeZone(Calendar.getInstance().getTimeZone());
        try
        {
            return formatter.parse(formatter.format(date));
        } catch (ParseException e)
        {
            return date;
        }
    }
}
