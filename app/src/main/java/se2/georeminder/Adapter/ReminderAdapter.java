package se2.georeminder.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.recyclerview.widget.RecyclerView;
import se2.georeminder.Database.ReminderEntity;
import se2.georeminder.R;

public class ReminderAdapter extends RecyclerView.Adapter<ReminderAdapter.ViewHolder>
{
    private List<ReminderEntity> reminders = new ArrayList<>();
    
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.reminder_recycler_item_layout, parent, false);
        return new ViewHolder(view);
    }
    
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position)
    {
        //*Selecting color reminders will be completed in later sprints
        //String color = reminders.get(position).getColor();
        holder.name.setText(reminders.get(position).getName());
        holder.description.setText(reminders.get(position).getDescription());
        holder.date.setText(reminders.get(position).getDate().toString());
        //holder.color.setText(color);
    }
    
    
    @Override
    public int getItemCount()
    {
        return reminders.size();
    }
    
    public void setReminders(List<ReminderEntity> reminders)
    {
        this.reminders = reminders;
        notifyDataSetChanged();
    }
    
    public ReminderEntity getReminderAt(int position)
    {
        return reminders.get(position);
    }
    
    class ViewHolder extends RecyclerView.ViewHolder
    {
        private TextView name, description, date;
        //*Selecting color reminders will be completed in later sprints
        //ImageView color;
        
        public ViewHolder(@NonNull View itemView)
        {
            super(itemView);
            name = itemView.findViewById(R.id.itemDetail_Name);
            description = itemView.findViewById(R.id.itemDetail_Description);
            date = itemView.findViewById(R.id.itemDetail_Date);
            //*Selecting color reminders will be completed in later sprints
            //color = itemView.findViewById(R.id.itemDetail_Color);
            
        }
    }
}
