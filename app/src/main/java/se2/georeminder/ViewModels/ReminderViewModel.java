package se2.georeminder.ViewModels;

import android.app.Application;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import se2.georeminder.Database.ReminderEntity;
import se2.georeminder.DatabaseRepository.DatabaseRepository;

// Provides a lifecycle abstraction layer that persists data being called in a activity or
//  fragment. This eliminates any memory leaks that may be created from running async tasks
//  that were called from within the activity/fragment that are no longer needed.
// When the screen is rotated or a system setting is modified, a fragment or activity must be
//  reloaded to reflect the new changes thus loosing and data stored on the fragment.
public class ReminderViewModel extends AndroidViewModel
{
    DatabaseRepository repo;
    private LiveData<List<ReminderEntity>> reminders;
    
    public ReminderViewModel(@NonNull Application application)
    {
        super(application);
        repo = new DatabaseRepository(application);
        reminders = repo.getAllReminders();
    }
    
    public void insertReminder(ReminderEntity reminder)
    {
        repo.insertReminder(reminder);
    }
    
    public void updateReminder(ReminderEntity reminder)
    {
        repo.updateReminder(reminder);
    }
    
    public void deleteReminder(ReminderEntity reminder)
    {
        repo.deleteReminder(reminder);
    }
    
    public LiveData<List<ReminderEntity>> getReminders()
    {
        return reminders;
    }
}
