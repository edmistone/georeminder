package se2.georeminder.Fragments;


import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.app.Fragment;
import android.os.SystemClock;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Calendar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import se2.georeminder.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class TimePickerFragments extends DialogFragment{

    private AlarmManager alarmMgr;
    private PendingIntent alarmIntent;
    public TimePickerDialog.OnTimeSetListener listener;

    public TimePickerFragments()
    {

    }

    public void setListener(final TimePickerDialog.OnTimeSetListener listener) {
        this.listener = listener;
    }




    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);
        InputFragment Input = (InputFragment) getFragmentManager().findFragmentByTag("IF");
        boolean is24Hour = DateFormat.is24HourFormat(getActivity());


        return new TimePickerDialog(getActivity(), Input, hour, minute, is24Hour);


    }

}
