package se2.georeminder.Fragments;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;

import android.os.SystemClock;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import se2.georeminder.Database.ReminderEntity;
import se2.georeminder.NotificationPublisher;
import se2.georeminder.R;
import se2.georeminder.ViewModels.ReminderViewModel;


public class InputFragment extends Fragment implements TimePickerDialog.OnTimeSetListener
{
    private ReminderViewModel reminderViewModel;
    public EditText name, description,
            address, distance;
    public String CHANNEL_ID = "Alarm_Notification";
     private View inputView;

    public InputFragment()
    {

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        inputView = inflater.inflate(R.layout.fragment_input, container, false);
        reminderViewModel = ViewModelProviders.of(this).get(ReminderViewModel.class);
        
        name = inputView.findViewById(R.id.reminderName);
        description = inputView.findViewById(R.id.reminderDescription);
        address = inputView.findViewById(R.id.reminderAddress);
        distance = inputView.findViewById(R.id.reminderDistance);




        Button alarmButton = (Button) inputView.findViewById(R.id.alarmButton);
        alarmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                TimePickerFragments timePicker = new TimePickerFragments();

              //  timePicker.setListener((TimePickerDialog.OnTimeSetListener)getActivity());

                timePicker.show(getFragmentManager().beginTransaction(), "time picker");




//                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getContext(), NotificationChannel.DEFAULT_CHANNEL_ID)
//                        .setSmallIcon(R.drawable.ic_alarm)
//                        .setContentTitle(name.getText().toString())
//                        .setContentText(description.getText().toString())
//                        .setPriority(NotificationCompat.PRIORITY_DEFAULT);
//
//                NotificationManagerCompat notificationManager = NotificationManagerCompat.from(getContext());
//                notificationManager.notify(1, mBuilder.build());
            }
        });
       // return view;

        Button submit = inputView.findViewById(R.id.addReminderButton);
        submit.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                saveReminder();
            }
        });
        return inputView;


    }



    @Override
    public void onTimeSet(TimePicker view, int hourSet, int minuteSet) {

        //TimePickerFragments TimePicker = new TimePickerFragments();

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());




        boolean is24Hour = DateFormat.is24HourFormat(getActivity());

        if(is24Hour)
        {
            calendar.set(Calendar.HOUR_OF_DAY, hourSet);
        }
        else
        {
            calendar.set(Calendar.HOUR, hourSet);
        }



        TextView textView =  inputView.findViewById(R.id.textView);
        textView.setText("Hour: " + hourSet + " Minute: " + minuteSet);


        calendar.set(Calendar.MINUTE, minuteSet);
        calendar.set(Calendar.SECOND, 0);



        scheduleNotification(getNotification(description.getText().toString()), calendar.getTimeInMillis());


    }



    private void scheduleNotification(Notification notification, long timeStamp) {

        Intent notificationIntent = new Intent(getActivity(), NotificationPublisher.class);
        notificationIntent.putExtra(NotificationPublisher.NOTIFICATION_ID, 1);
        notificationIntent.putExtra(NotificationPublisher.NOTIFICATION, notification);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getActivity(), 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        //long futureInMillis = SystemClock.elapsedRealtime() + delay;
        AlarmManager alarmManager = (AlarmManager) getActivity().getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, timeStamp, pendingIntent);

    }


    private Notification getNotification(String content) {




        Intent intent = new Intent(getActivity(), NotificationPublisher.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(getActivity(), 0, intent, 0);



        Notification.Builder builder = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            builder = new Notification.Builder(getActivity(), CHANNEL_ID);

            Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

            builder.setContentTitle(name.getText().toString());
            builder.setContentText(content);
            builder.setSmallIcon(R.drawable.ic_alarm);
            builder.setContentIntent(pendingIntent);
            builder.setSound(uri);
            builder.setAutoCancel(true);

            return builder.build();

        }
        else
        {
            Toast.makeText(getActivity(), "Error!", Toast.LENGTH_LONG).show();
            return null;
        }

    }



    private void saveReminder()
    {
        String name = this.name.getText().toString();
        String description = this.description.getText().toString();
        String address = this.address.getText().toString();
        String color = "#FFFFFF";
        String distance = this.distance.getText().toString();
        
        if(name.trim().isEmpty() || description.trim().isEmpty() || address.trim().isEmpty() ||
                color.trim().isEmpty() || distance.trim().isEmpty())
        {
            Toast.makeText(getContext(),
                    "Please complete all fields.", Toast.LENGTH_LONG).show();
            return;
        }
        
        reminderViewModel.insertReminder(new ReminderEntity(name, description, color,
                address, Float.valueOf(distance), Calendar.getInstance().getTime()));
        Toast.makeText(getContext(), "New Reminder Created", Toast.LENGTH_LONG).show();


    }

}//end class



