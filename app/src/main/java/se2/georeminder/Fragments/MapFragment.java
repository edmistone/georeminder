package se2.georeminder.Fragments;


import android.app.AlarmManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.Manifest;

import android.content.pm.PackageManager;

import com.mapbox.api.geocoding.v5.MapboxGeocoding;
import com.mapbox.api.geocoding.v5.models.CarmenFeature;
import com.mapbox.api.geocoding.v5.models.GeocodingResponse;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.maps.MapView;
// classes needed to add location layer
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import android.location.Location;
import android.widget.Button;
import android.widget.Toast;
import com.mapbox.mapboxsdk.geometry.LatLng;

import com.mapbox.mapboxsdk.plugins.locationlayer.LocationLayerPlugin;
import com.mapbox.mapboxsdk.plugins.locationlayer.modes.CameraMode;
import com.mapbox.mapboxsdk.plugins.locationlayer.modes.RenderMode;
import com.mapbox.services.android.navigation.ui.v5.NavigationLauncherOptions;
import com.mapbox.android.core.location.LocationEngine;
import com.mapbox.android.core.location.LocationEnginePriority;
import com.mapbox.android.core.location.LocationEngineProvider;
import com.mapbox.android.core.location.LocationEngineListener;
import com.mapbox.android.core.permissions.PermissionsListener;
import com.mapbox.android.core.permissions.PermissionsManager;
import android.location.Location;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import se2.georeminder.Database.ReminderEntity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import se2.georeminder.NotificationPublisher;
import se2.georeminder.R;
import se2.georeminder.ViewModels.ReminderViewModel;

public class MapFragment extends Fragment implements PermissionsListener, OnMapReadyCallback, LocationEngineListener {
    DecimalFormat format = new DecimalFormat("#.#");
    public String CHANNEL_ID = "Alarm_Notification";
    private MapView mapView;
    // variables for adding location layer
    private MapboxMap mapboxMap;
    private PermissionsManager permissionsManager;
    private LocationLayerPlugin locationLayerPlugin;
    private LocationEngine locationEngine;
    private Location originLocation;
    private ReminderViewModel reminderViewModel;
    private ArrayList<LatLng> reminderLocations = new ArrayList<>();
    private ArrayList<ReminderEntity> reminderObjects = new ArrayList<>();
    private ArrayList<Boolean> ReminderTriggeredThisRun = new ArrayList<>();
    float[] distance = new float[1];
    private LocationListener locationListenerGPS = new LocationListener() {
        public void onLocationChanged(Location location) {
            double longitudeNetwork = location.getLongitude();
            double latitudeNetwork = location.getLatitude();

            for (int i = 0; i < reminderLocations.size(); i++) {

                 Location.distanceBetween(latitudeNetwork, longitudeNetwork, reminderLocations.get(i).getLatitude(),
                         reminderLocations.get(i).getLongitude(), distance);
                 distance[0] = distance[0] * 0.000621371f;
                 if(distance[0] <= reminderObjects.get(i).getDistance())
                 {
                     if(!ReminderTriggeredThisRun.get(i))
                     {
                         String title = format.format(distance[0]) + " miles away from " + reminderObjects.get(i).getName();
                         scheduleNotification(getNotification(title,
                                 reminderObjects.get(i).getDescription()), System.currentTimeMillis());
                         ReminderTriggeredThisRun.set(i,true);
                         break;
                     }
                 }
            }
            
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {

                }
            });
        }
        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {
        }
        @Override
        public void onProviderEnabled(String s) {
        }
        @Override
        public void onProviderDisabled(String s) {
        }
    };
    LocationManager locationManager;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        Mapbox.getInstance(getActivity(), "pk.eyJ1IjoiZWRtaXN0b25lIiwiYSI6ImNqbTVhYXIybjNrejMzcGxpc3dxeXV5c3AifQ.daP65Dq01la86I7qSuGqFA");

        View view = inflater.inflate(R.layout.fragment_map, container, false);

        Mapbox.getInstance(getActivity(), "pk.eyJ1IjoiZWRtaXN0b25lIiwiYSI6ImNqbTVhYXIybjNrejMzcGxpc3dxeXV5c3AifQ.daP65Dq01la86I7qSuGqFA");
        mapView = view.findViewById(R.id.map_View);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);

        reminderViewModel = ViewModelProviders.of(this).get(ReminderViewModel.class);

        locationManager = (LocationManager)getActivity().getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED)
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 0f, locationListenerGPS);
        return view;
    }

    @Override
    public void onMapReady(MapboxMap mapboxMap) {

        this.mapboxMap = mapboxMap;
        //enableLocationPlugin();
        locationEnable();
        mapboxMap.getUiSettings().setZoomControlsEnabled(true);
        mapboxMap.getUiSettings().setZoomGesturesEnabled(true);
        mapboxMap.getUiSettings().setScrollGesturesEnabled(true);
        mapboxMap.getUiSettings().setAllGesturesEnabled(true);
        reminderViewModel.getReminders().observe(this, new Observer<List<ReminderEntity>>()
        {
            @Override
            public void onChanged(List<ReminderEntity> reminderEntities)
            {
                for(int i =0; i < reminderEntities.size(); i++)
                {
                    int index = i;
                    String query = reminderEntities.get(i).getAddress();
                    String name = reminderEntities.get(i).getName();
                    String description = reminderEntities.get(i).getDescription();

                    MapboxGeocoding client = MapboxGeocoding.builder()
                            .accessToken("pk.eyJ1IjoiZWRtaXN0b25lIiwiYSI6ImNqbTVhYXIybjNrejMzcGxpc3dxeXV5c3AifQ.daP65Dq01la86I7qSuGqFA")
                            .query(query)
                            .proximity(Point.fromLngLat(originLocation.getLongitude(), originLocation.getLatitude()))
                            .country(Locale.US)
                            .languages(Locale.ENGLISH)
                            .geocodingTypes("poi", "address")
                            .autocomplete(true)
                            .build();

                    client.enqueueCall(new Callback<GeocodingResponse>() {
                        @Override
                        public void onResponse(Call<GeocodingResponse> call, Response<GeocodingResponse> response) {
                            List<CarmenFeature> results = response.body().features();

                            if(results.size() > 0)
                            {
                                Point firstResultPoint = results.get(0).center();
                                LatLng testing = new LatLng(firstResultPoint.latitude(), firstResultPoint.longitude());
                                reminderLocations.add(testing);
                                reminderObjects.add(reminderEntities.get(index));
                                ReminderTriggeredThisRun.add(false);
                                mapboxMap.addMarker(new MarkerOptions()
                                        .position(new LatLng(testing.getLatitude(), testing.getLongitude()))
                                        .title(name)
                                        .snippet(description));
                            }
                        }

                        @Override
                        public void onFailure(Call<GeocodingResponse> call, Throwable t) {
                            t.printStackTrace();
                        }
                    });
                }
            }
        });

    }

    void locationEnable() {
        if (PermissionsManager.areLocationPermissionsGranted(getActivity())) {
            intialLocationEngine();
            intializLocationLayer();
        } else {
            permissionsManager = new PermissionsManager(this);
            permissionsManager.requestLocationPermissions(getActivity());
        }
    }

    void intialLocationEngine() {
        locationEngine = new LocationEngineProvider(getActivity()).obtainBestLocationEngineAvailable();
        locationEngine.setPriority(LocationEnginePriority.HIGH_ACCURACY);
        locationEngine.activate();
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        Location lastLocation = locationEngine.getLastLocation();
        if (lastLocation != null) {
            originLocation = lastLocation;
            setCamerpostion(lastLocation);
        } else {
            locationEngine.addLocationEngineListener(this);
        }

    }

    void intializLocationLayer() {
        locationLayerPlugin = new LocationLayerPlugin(mapView, mapboxMap);
        locationLayerPlugin.setLocationLayerEnabled(true);
        locationLayerPlugin.setCameraMode(CameraMode.TRACKING_GPS);
        locationLayerPlugin.setRenderMode(RenderMode.GPS);
    }

    void setCamerpostion(Location camerpostion) {
        mapboxMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(camerpostion.getLatitude(), camerpostion.getLongitude()), 18));
        CameraPosition position = new CameraPosition.Builder()
                .target(new LatLng(camerpostion.getLatitude(), camerpostion.getLongitude()))
                .zoom(15)
                .build();
        mapboxMap.setCameraPosition(position);
    }

    @SuppressWarnings( {"MissingPermission"})
    private void enableLocationPlugin() {
        // Check if permissions are enabled and if not request
        if (PermissionsManager.areLocationPermissionsGranted(getActivity())) {
            initializeLocationEngine();
            // Create an instance of the plugin. Adding in LocationLayerOptions is also an optional
            // parameter
            LocationLayerPlugin locationLayerPlugin = new LocationLayerPlugin(mapView, mapboxMap);

            // Set the plugin's camera mode
            locationLayerPlugin.setCameraMode(CameraMode.TRACKING);
            //getLifecycle().addObserver(locationLayerPlugin);
        } else {
            permissionsManager = new PermissionsManager( this);
            permissionsManager.requestLocationPermissions(getActivity());
        }
    }

    @SuppressWarnings( {"MissingPermission"})
    private void initializeLocationEngine() {
        LocationEngineProvider locationEngineProvider = new LocationEngineProvider(getActivity());
        locationEngine = locationEngineProvider.obtainBestLocationEngineAvailable();
        locationEngine.setPriority(LocationEnginePriority.HIGH_ACCURACY);
        locationEngine.activate();

        Location lastLocation = locationEngine.getLastLocation();
        if (lastLocation != null) {
            originLocation = lastLocation;
        } else {
            locationEngine.addLocationEngineListener(this);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        permissionsManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onExplanationNeeded(List<String> permissionsToExplain) {
        Toast.makeText(getActivity(), R.string.user_location_permission_explanation, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onPermissionResult(boolean granted) {
        if (granted) {
            locationEnable();
        } else {
            Toast.makeText(getActivity(), R.string.user_location_permission_not_granted, Toast.LENGTH_LONG).show();
            getActivity().finish();
        }
    }

    @SuppressWarnings( {"MissingPermission"})
    @Override
    public void onStart() {
        super.onStart();
        if(locationEngine != null)
        {
            locationEngine.requestLocationUpdates();
        }
        mapView.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
        if (locationLayerPlugin != null) {
            locationLayerPlugin.onStop();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mapView.onDestroy();
        locationManager.removeUpdates(locationListenerGPS);
        locationManager = null;
        locationListenerGPS = null;
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onConnected() {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        locationEngine.requestLocationUpdates();
    }

    @Override
    public void onLocationChanged(Location location) {
        Toast.makeText(getActivity(), R.string.user_location_permission_explanation, Toast.LENGTH_LONG).show();
        if (location != null) {
            originLocation = location;
            setCamerpostion(location);

        }
    }

    private void scheduleNotification(Notification notification, long timeStamp) {

        Intent notificationIntent = new Intent(getActivity(), NotificationPublisher.class);
        notificationIntent.putExtra(NotificationPublisher.NOTIFICATION_ID, 1);
        notificationIntent.putExtra(NotificationPublisher.NOTIFICATION, notification);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getActivity(), 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        //long futureInMillis = SystemClock.elapsedRealtime() + delay;
        AlarmManager alarmManager = (AlarmManager) getActivity().getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, timeStamp, pendingIntent);

    }


    private Notification getNotification(String title, String content) {




        Intent intent = new Intent(getActivity(), NotificationPublisher.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(getActivity(), 0, intent, 0);



        Notification.Builder builder = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            builder = new Notification.Builder(getActivity(), CHANNEL_ID);

            Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

            builder.setContentTitle(title);
            builder.setContentText(content);
            builder.setSmallIcon(R.drawable.ic_alarm);
            builder.setContentIntent(pendingIntent);
            builder.setSound(uri);
            builder.setAutoCancel(true);

            return builder.build();

        }
        else
        {
            Toast.makeText(getActivity(), "Error!", Toast.LENGTH_LONG).show();
            return null;
        }

    }
}
