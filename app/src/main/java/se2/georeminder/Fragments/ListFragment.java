package se2.georeminder.Fragments;

import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import se2.georeminder.Adapter.ReminderAdapter;
import se2.georeminder.Database.ReminderEntity;
import se2.georeminder.MainActivity;
import se2.georeminder.R;
import se2.georeminder.ViewModels.ReminderViewModel;

public class ListFragment extends Fragment
{
    private ReminderViewModel reminderViewModel;
    private int simpleCounter = 0;
    
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState)
    {
        //Inflates the list fragment and sets it as a new view so we can implement a layout manager
        View view = inflater.inflate(R.layout.fragment_list, container, false);
        
        //Retrieve the recyclerView from the xml frag and assign it a linear layoutManager
        //  which specifies how the recycler should organize incoming data.
        RecyclerView recyclerView = view.findViewById(R.id.reminderRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setHasFixedSize(true);
        
        ReminderAdapter adapter = new ReminderAdapter();
        recyclerView.setAdapter(adapter);
        
        reminderViewModel = ViewModelProviders.of(this).get(ReminderViewModel.class);
        reminderViewModel.getReminders().observe(this, new Observer<List<ReminderEntity>>()
        {
            @Override
            public void onChanged(List<ReminderEntity> reminderEntities)
            {
                adapter.setReminders(reminderEntities);
                simpleCounter++;
                if(simpleCounter == 1)
                {
                    //Notify user of the database query. May take a second depending on number of entries.
                    Toast.makeText(getContext(), "Fetching Reminders...", Toast.LENGTH_SHORT).show();
                }
            }
        });
        
        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0,
                ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT)
        {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target)
            {
                return false;
            }
    
            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction)
            {
                reminderViewModel.deleteReminder(adapter.getReminderAt(viewHolder.getAdapterPosition()));
                Toast.makeText(getContext(), "Reminder Removed", Toast.LENGTH_LONG).show();
            }
        }).attachToRecyclerView(recyclerView);

        return view;
    }
    
}
