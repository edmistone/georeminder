package se2.georeminder.DatabaseRepository;

import android.app.Application;
import android.os.AsyncTask;

import java.util.List;

import androidx.lifecycle.LiveData;
import se2.georeminder.Database.IReminderDAO;
import se2.georeminder.Database.ReminderDatabase;
import se2.georeminder.Database.ReminderEntity;

//Abstraction repository helps running different queries off the main thread while also giving
//  a place for outside data sources should one arise.
public class DatabaseRepository
{
    private IReminderDAO reminderDAO;
    private LiveData<List<ReminderEntity>> allReminders;
    
    public DatabaseRepository(Application application)
    {
        ReminderDatabase db = ReminderDatabase.getInstance(application);
        reminderDAO = db.ReminderDAO();
        allReminders = reminderDAO.getAllReminders();
    }
    
    // Abstraction layer methods that automatically run the queries in async to avoid any
    //  app freezes and maintainability
    public void insertReminder(ReminderEntity reminder)
    {
        new InsertReminderAsyncTask(reminderDAO).execute(reminder);
    }
    
    public  void updateReminder(ReminderEntity reminder)
    {
        new UpdateReminderAsyncTask(reminderDAO).execute(reminder);
    }
    
    public void deleteReminder(ReminderEntity reminder)
    {
        new DeleteReminderAsyncTask(reminderDAO).execute(reminder);
    }
    
    public LiveData<List<ReminderEntity>> getAllReminders()
    {
        return allReminders;
    }
    
    //----------------------------------------------------------------------------------------------
    //  Defines methods to run queries on background threads.
    private static class InsertReminderAsyncTask extends AsyncTask<ReminderEntity, Void, Void>
    {
        private IReminderDAO reminderDAO;
        
        private InsertReminderAsyncTask(IReminderDAO reminderDAO)
        {
            this.reminderDAO = reminderDAO;
        }
        
        @Override
        protected Void doInBackground(ReminderEntity... reminderEntities)
        {
            reminderDAO.addReminder(reminderEntities[0]);
            return null;
        }
    }
    
    private static class UpdateReminderAsyncTask extends AsyncTask<ReminderEntity, Void, Void>
    {
        private IReminderDAO reminderDAO;
        
        private UpdateReminderAsyncTask(IReminderDAO reminderDAO)
        {
            this.reminderDAO = reminderDAO;
        }
        
        @Override
        protected Void doInBackground(ReminderEntity... reminderEntities)
        {
            reminderDAO.updateReminder(reminderEntities[0]);
            return null;
        }
    }
    
    private static class DeleteReminderAsyncTask extends AsyncTask<ReminderEntity, Void, Void>
    {
        private IReminderDAO reminderDAO;
        
        private DeleteReminderAsyncTask(IReminderDAO reminderDAO)
        {
            this.reminderDAO = reminderDAO;
        }
        
        @Override
        protected Void doInBackground(ReminderEntity... reminderEntities)
        {
            reminderDAO.deleteReminder(reminderEntities[0]);
            return null;
        }
    }
}
